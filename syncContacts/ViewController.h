//
//  ViewController.h
//  syncContacts
//
//  Created by Juan on 1/16/15.
//  Copyright (c) 2015 circles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsManager.h"

@interface ViewController : UIViewController <ContactsManagerDelegate>
@property(nonatomic, strong) IBOutlet UILabel *statusLabel;
@property(nonatomic, strong) IBOutlet UIButton *syncButton;
@property(nonatomic, strong) IBOutlet UITextView *logTextView;

-(IBAction)syncContacts:(id)sender;
@end

