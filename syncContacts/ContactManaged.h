//
//  ContactManaged.h
//  syncContacts
//
//  Created by Juan on 1/16/15.
//  Copyright (c) 2015 circles. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ContactManaged : NSManagedObject
@property(nonatomic, strong) NSNumber *identification;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSNumber *visible;
@end
