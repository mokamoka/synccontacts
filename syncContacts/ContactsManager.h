//
//  Contacts.h
//  syncContacts
//
//  Created by Juan on 1/16/15.
//  Copyright (c) 2015 circles. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ContactsManager;

@protocol ContactsManagerDelegate <NSObject>
@required
-(void)contactsManagerAddressBookGrantedAuthorization;
-(void)contactsManagerAddressBookNotGrantedAuthorization;
-(void)contactsManagerAddressBookNotDeterminedAuthorization;
-(void)contactsManagerSyncFinish:(NSArray*)changes;
-(void)contactsManagerSyncFail;
//@optional

@end

@interface ContactsManager : NSObject {
    __unsafe_unretained id <ContactsManagerDelegate> delegate;
}

@property(nonatomic, assign) id<ContactsManagerDelegate> delegate;

-(void)checkAddressBookAccess;
-(void)requestAddressBookAccess;
-(void)syncContacts;

@end



