//
//  ViewController.m
//  syncContacts
//
//  Created by Juan on 1/16/15.
//  Copyright (c) 2015 circles. All rights reserved.
//

#import "ViewController.h"

#import "ContactsManager.h"

@interface ViewController ()
@property(nonatomic,strong) ContactsManager *cm;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.cm = [[ContactsManager alloc] init];
    [self.cm setDelegate:self];

    [self.cm checkAddressBookAccess];
    
}
-(IBAction)syncContacts:(id)sender{
    
    [self.statusLabel setText:@"Syncing is in wait"];
    [self.syncButton setHidden:YES];
    
    [self.logTextView setText:@""];
    
    [self.cm syncContacts];

}
#pragma mark - ContactsManagerDelegate
-(void)contactsManagerAddressBookGrantedAuthorization{
    [self.statusLabel setText:@""];
    [self.syncButton setHidden:NO];
}
-(void)contactsManagerAddressBookNotGrantedAuthorization{
    
    [[[UIAlertView alloc ] initWithTitle:@"Address Book Authorization Status"
                                 message:@"Denied - Please go to Settings -> Privacy -> Contacts and allow the app"
                                delegate:self
                       cancelButtonTitle:@"Ok"
                       otherButtonTitles: nil] show];
    
    [self.statusLabel setText:@"Address Book Auth: Denied"];
    [self.syncButton setHidden:YES];
}
-(void)contactsManagerAddressBookNotDeterminedAuthorization{
    [self.cm requestAddressBookAccess];
}

-(void)contactsManagerSyncFinish:(NSArray*)changes{
    
    [self.logTextView setText:@""];
    
    if ([changes count]>0){
        for (NSString *logLine in changes) {
            [self.logTextView setText:[NSString stringWithFormat:@"%@\n>%@",self.logTextView.text, logLine]];
        }
    }else{
        [self.logTextView setText:@"No changes"];
    }
    [self.statusLabel setText:@"Sync is done"];
    [self.syncButton setHidden:NO];
}
-(void)contactsManagerSyncFail{
    
    [self.statusLabel setText:@"Sync is done with errors"];
    [self.syncButton setHidden:NO];
    
    [[[UIAlertView alloc ] initWithTitle:@"Something go wrong during sync"
                                 message:@"Something go wrong during sync saving"
                                delegate:self
                       cancelButtonTitle:@"Ok"
                       otherButtonTitles: nil] show];
    
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
