//
//  Contacts.m
//  syncContacts
//
//  Created by Juan on 1/16/15.
//  Copyright (c) 2015 circles. All rights reserved.
//

#import "ContactsManager.h"
#import "ContactManaged.h"

#import "AppDelegate.h"

#import "ContactManaged.h"

@import AddressBook;

@implementation ContactsManager

@synthesize delegate;
NSManagedObjectContext *context;

-(id)init{
    
    if (self = [super init]){
        
        AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        context = [ad managedObjectContext];
        
        //AddressBook changes trigger
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, nil);
        ABAddressBookRegisterExternalChangeCallback(addressBook, addressBookChangeCallback, (__bridge void *)(self));
    }
    
    return self;
}

void addressBookChangeCallback(ABAddressBookRef reference, CFDictionaryRef info, void *context){
    [(__bridge ContactsManager *)context addressBookDidChangeExternally];
}

//This function is call when something has change on Contacts if the app is open
-(void)addressBookDidChangeExternally{
    [self syncContacts];
}

-(void)checkAddressBookAccess{
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        
        [self.delegate contactsManagerAddressBookNotGrantedAuthorization];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
        
        [self.delegate contactsManagerAddressBookNotDeterminedAuthorization];
        
    } else {
        
        [self.delegate contactsManagerAddressBookGrantedAuthorization];
        
    }
}

-(void)requestAddressBookAccess{

    ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
        if (!granted){
            [self.delegate contactsManagerAddressBookNotGrantedAuthorization];
            return;
        }
        [self.delegate contactsManagerAddressBookGrantedAuthorization];
    });
}

-(void)syncContacts{
    
    [self markAllContactsAsInvisible];
    
    NSMutableArray *changes = [[NSMutableArray alloc] init];
    
    ABAddressBookRef contactBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRef allPeople = contactBook;
    CFArrayRef allContacts = ABAddressBookCopyArrayOfAllPeople(allPeople);
    CFIndex numberOfContacts  = ABAddressBookGetPersonCount(allPeople);
    
    for(int i = 0; i < numberOfContacts; i++){
    
        NSNumber *personId = nil;
        NSString *personName = @"";
        NSString *personPhone = @"";
        
        ABRecordRef aPerson = CFArrayGetValueAtIndex(allContacts, i);
        
        ABRecordID recordIdProperty = ABRecordGetRecordID(aPerson);
        ABMultiValueRef fnameProperty = ABRecordCopyValue(aPerson, kABPersonFirstNameProperty);
        ABMultiValueRef lnameProperty = ABRecordCopyValue(aPerson, kABPersonLastNameProperty);
        ABMultiValueRef phoneProperty = ABRecordCopyValue(aPerson, kABPersonPhoneProperty);
        
        //kABPersonModificationDateProperty usefull to detect updates, but I'm not using it now because I need to detect removals.
        
        NSArray *phoneArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(phoneProperty);
        
        if (fnameProperty != nil) {
            personName = [NSString stringWithFormat:@"%@", fnameProperty];
        }
        if (lnameProperty != nil) {
            personName = [personName stringByAppendingString:[NSString stringWithFormat:@" %@", lnameProperty]];
        }
        
        if ([phoneArray count] > 0) {
            if ([phoneArray count] > 1) {
                for (int i = 0; i < [phoneArray count]; i++) {
                    personPhone = [personPhone stringByAppendingString:[NSString stringWithFormat:@"%@, ", [[[phoneArray objectAtIndex:i] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""]]];
                }
            }else {
                personPhone = [NSString stringWithFormat:@"%@", [[[phoneArray objectAtIndex:0] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""]];
            }
        }
        
        personId = [NSNumber numberWithDouble:recordIdProperty];
        
        ContactManaged *cm = [self getContactByIdentification:personId];
        
        if (cm==nil){
            
            //Add new contact
            [self addContact:personId withName:personName withPhone:personPhone];
            [changes addObject:[NSString stringWithFormat:@"New record: %@ %@", personName, personPhone]];
            
        }else{
            
            //Already exist, something change?
            if (![cm.name isEqualToString:personName]){
                [changes addObject:[NSString stringWithFormat:@"ID:%@ Name change from %@ to %@", personId, cm.name, personName]];
                
                //Update the record
                [cm setName:personName];
            }
            if (![cm.phone isEqualToString:personPhone]){
                [changes addObject:[NSString stringWithFormat:@"ID:%@ (%@) Phone change from %@ to %@", personId, cm.name, cm.phone, personPhone]];
                
                //Update the record
                [cm setPhone:personPhone];
            }
            
            [cm setVisible:[NSNumber numberWithBool:YES]];
            
        }
        
    }
    
    NSArray *deleteContacts = [self deleteInvisible];
    for (ContactManaged *dc in deleteContacts) {
        [changes addObject:[NSString stringWithFormat:@"ID:%@ Deleted (%@)", dc.identification, dc.name]];
    }
    
    if ([self saveChanges]){
        [self.delegate contactsManagerSyncFinish:[NSArray arrayWithArray:changes]];
    }else{
        [self.delegate contactsManagerSyncFail];
    }
    
    
}
-(ContactManaged*)getContactByIdentification:(NSNumber*)contactId{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"(identification = %f)", [contactId doubleValue]]];
    
    NSArray *contacts = [context executeFetchRequest:request error:nil];
    if ([contacts count] > 0){
        return (ContactManaged*)[contacts objectAtIndex:0];
    }else{
        return  nil;
    }
    
}
-(void)addContact:(NSNumber*)identification withName:(NSString*)name withPhone:(NSString*)phone{
    
    ContactManaged *cm = [NSEntityDescription insertNewObjectForEntityForName:@"Contact"
                                                       inManagedObjectContext:context];
    
    
    [cm setIdentification:identification];
    [cm setName:name];
    [cm setPhone:phone];
    [cm setVisible:[NSNumber numberWithBool:YES]];
    
}
-(bool)saveChanges{
    
    NSError *error;
    
    return [context save:&error];
    
}
-(void)markAllContactsAsInvisible{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context]];
    
    NSArray *contacts = [context executeFetchRequest:request error:nil];
    for (ContactManaged *cm in contacts) {
        [cm setVisible:[NSNumber numberWithBool:NO]];
    }
    
}
-(NSArray*)deleteInvisible{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Contact" inManagedObjectContext:context]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"(visible = 0)"]];
    
    NSArray *deletedContacts = [context executeFetchRequest:request error:nil];
    for (ContactManaged *cm in deletedContacts) {
        [context deleteObject:cm];
    }
    
    return deletedContacts;
    
}
@end
